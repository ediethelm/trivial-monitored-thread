;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :trivial-monitored-thread
  :name "trivial-monitored-thread"
  :description "Trivial Monitored Thread offers a very simple (aka trivial) way of spawning threads and being informed when one any of them crash and die."
  :version "0.3.11"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-utilities
	       :log4cl
	       :iterate)
  :components ((:file "package")
	       (:file "trivial-monitored-thread")))

(defsystem :trivial-monitored-thread/test
  :name "trivial-monitored-thread/test"
  :description "Unit Tests for the trivial-monitored-thread project."
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-monitored-thread fiveam)
  :perform (test-op (o s) (uiop:symbol-call :fiveam  '#:run! :trivial-monitored-thread-tests))
  :components ((:file "test-trivial-monitored-thread")))

