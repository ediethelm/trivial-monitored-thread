
(in-package :trivial-monitored-thread)

(defclass monitored-thread ()
  ((thread :initform nil
	   :initarg :thread
	   :accessor raw-thread)
   (id :initform nil
       :initarg :id
       :accessor thread-id)
   (body :initform #'(lambda ())
	 :initarg :body
	 :reader thread-body)
   (sleep-duration :accessor sleep-duration
		   :type fixnum)
   (shutdowndown-requested :accessor shutdowndown-requested
			   :initform nil)
   (finished :initform nil
	     :accessor finished)
   (running :initform nil
	    :accessor running)
   (restart-counter :initform 0
		    :accessor restart-counter)
   (max-restarts :initarg :max-restarts
		 :accessor max-restarts)
   (args :initarg :args
	 :accessor thread-args)))

(defmethod thread-name ((thread monitored-thread))
  (bt:thread-name (raw-thread thread)))

(defmethod print-object ((thread monitored-thread) stream)
  (print-unreadable-object (thread stream :type t)
    (format stream (thread-name thread))
    (when (finished thread)
      (format stream " [finished]"))))

(defclass thread-monitor ()
  ((all-threads :accessor all-threads
		:initform '())
   (thread-last-call :accessor thread-last-call
		     :initform '())
   (total-monitored-threads :accessor total-threads
			    :initform 0)
   (monitor-thread :reader monitor-thread
		   :initform nil)))

(defvar thread-monitor nil)
(defvar *shutdown-requested* nil)

(defun start-thread-monitor ()
  "Initialize the underlying engine for monitoring threads.  
*This function MUST be called before any other function from this namespace.*"
  (setf *shutdown-requested* nil)
  (if thread-monitor
      (log:warn "Thread Monitor already initialized. Skipping initialization.")
      (progn
	(setf thread-monitor (make-instance 'thread-monitor))
	(setf (slot-value thread-monitor 'monitor-thread)
	      (bt:make-thread #'monitor-all-threads :name "Thread Monitor"))
	(log:info "Thread Monitor initialized."))))

(defun stop-thread-monitor (&key (kill-all nil))
  "Stops the thread monitor. Any thread that is not also stopped by (:kill-all t) will continue unmonitored with potentially unpredictable consequences.  
*kill-all* - stop all monitored threads before exiting. Might kill threads that take too long (> 3 seconds) to finish."
  (setf *shutdown-requested* t)

  (unless thread-monitor
    (log:warn "The Thread Monitor appears not to have been correctly initialized. Ignoring this shutdown request.")
    (return-from stop-thread-monitor))

  (when kill-all
    (iterate:iterate
      (iterate:for th in (all-threads thread-monitor))
      (stop-thread (thread-id th))
      (join-thread (thread-id th))))

  (bordeaux-threads:join-thread (monitor-thread thread-monitor))
  (setf thread-monitor nil)
  (return-from stop-thread-monitor))

(defparameter on-dead-thread-fn #'(lambda (thread-id thread-args restart-counter max-restarts will-restart-p)
				    (declare (ignore thread-args restart-counter max-restarts))
				    (trivial-utilities:aif (get-monitored-thread thread-id)
							   (log:warn "Thread '~a' (ID ~a) died. It will ~:[not ~;~]be restarted."
								     thread-id
								     (bt:thread-name (raw-thread it))
								     will-restart-p))))
(defun register-on-dead-thread (fn)
  "Set the function *fn* to be called when a monitored thread dies.
The function must implement following lambda-list: (thread-id thread-args restart-counter max-restarts will-restart-p)."
  (setf on-dead-thread-fn fn))

(defun monitor-all-threads ()
  (iterate:iterate
    (iterate:until *shutdown-requested*)
    (let* ((now (get-universal-time))
	   (dead-threads (iterate:iterate
			   (iterate:for thread in (all-threads thread-monitor))
			   (when (and (not (shutdowndown-requested thread))
				      (or (and (raw-thread thread)
					       (not (bt:thread-alive-p (raw-thread thread))))
					  (and (cdr (assoc (thread-id thread) (thread-last-call thread-monitor)))
					       (< (cdr (assoc (thread-id thread) (thread-last-call thread-monitor))) now))))
			     (iterate:collect thread)))))
      (iterate:iterate
	(iterate:for thread in dead-threads)
	(funcall on-dead-thread-fn
		 (thread-id thread)
		 (thread-args thread)
		 (restart-counter thread)
		 (max-restarts thread)
		 (<= (restart-counter thread)
		     (max-restarts thread)))
	(setf (thread-last-call thread-monitor) (delete thread (thread-last-call thread-monitor) :key #'car))
	(setf (all-threads thread-monitor)  (delete thread (all-threads thread-monitor)))
	(log:error "The thread '~a' (id ~a) is dead (raw thread alive: ~a) after ~a restarts. Expected ~a but now is ~a."
		   (thread-name thread)
		   (thread-id thread)
		   (and (raw-thread thread)
			(bt:thread-alive-p (raw-thread thread)))
		   (restart-counter thread)
		   (cdr (assoc (thread-id thread) (thread-last-call thread-monitor)))
		   now)))
    (sleep 5)))

(defun inform-thread-alive (thread-id next-call)
  (trivial-utilities:aif (assoc thread-id (thread-last-call thread-monitor))
			 (setf (cdr it) next-call)
			 (push (cons thread-id next-call) (thread-last-call thread-monitor)))
  (return-from inform-thread-alive))

(defun add-thread-to-monitor ()
  "Returns the new monitored thread"
  (trivial-utilities:aprog1
      (make-instance 'monitored-thread :id (incf (total-threads thread-monitor))) ;; @TODO Should add a mutex to protect total-threads
    (push it (all-threads thread-monitor))))

(defun get-monitored-thread (thread-id)
  (car (member thread-id (all-threads thread-monitor) :key #'thread-id)))

(defun thread-alive (thread-id)
  (trivial-utilities:awhen (get-monitored-thread thread-id)
    (and (bt:thread-alive-p (raw-thread it))
	 (>= (cdr (assoc (thread-id it) (thread-last-call thread-monitor))) (get-universal-time)))))

(defun start-thread (thread-id)
  (declare (ignore thread-id))
  )

(defun stop-thread (thread-id)
  "Given the valid id of a monitored thread request it to shutdown.  
*thread-id* - The id of the monitored thread"
  (trivial-utilities:awhen (get-monitored-thread thread-id)
    (setf (shutdowndown-requested it) t)))

(defun join-thread (thread-id)
  "Given the valid id of a monitored thread wait until it finishes.  
*thread-id* - The id of the monitored thread"
  (trivial-utilities:awhen (get-monitored-thread thread-id)
    (trivial-utilities:awhen (raw-thread it)
      (bt:join-thread it))))

(defparameter *debug-conditions* nil)

(defun enable-condition-debugging ()
  (setf *debug-conditions* t))

(defun disable-condition-debugging ()
  (setf *debug-conditions* nil))

(defmacro make-monitored-thread ((&key name sleep-duration (max-restarts 5) args thread-initialization thread-cleanup restart-initialization restart-cleanup) &body body)
  "Define and register a new monitored thread. The *body* of the thread will be executed in a loop. The thread will be started immediately.  
*name* - A identifies string for the thread  
*sleep-duration* - Define the time duration to yield between iterations (this will be cut to a maximum of 1 second)  
*max-restarts* - Number of times this thread should be restarted  
*args* - freely definable payload to be carried to the 'on-dead-thread' function  
*thread-initialization* - Code to be executed once before first thread start  
*thread-cleanup* - Code to be executed after last thread stop  
*restart-initialization* - Code to be executed before each thread restart  
*restart-cleanup* - Code to be executed after last thread restart  
*body* - Code to be executed repeatedly (this must not block)"
  (trivial-utilities:once-only (sleep-duration)
  `(let ((this-thread (add-thread-to-monitor)))
     (setf (trivial-monitored-thread:max-restarts this-thread) ,max-restarts)
     (setf (trivial-monitored-thread:thread-args this-thread) ,args)
     (setf (slot-value this-thread 'body)
	   #'(lambda ()
	       ;; Inject restart-independent initialization code
	       ,@(when thread-initialization
		   (list thread-initialization))
	       
	       (iterate:iterate
		 (iterate:while (and (<= (trivial-monitored-thread:restart-counter this-thread)
					 (trivial-monitored-thread:max-restarts this-thread))
				     (not (trivial-monitored-thread:shutdowndown-requested this-thread))))
		 
		 ;; Inject restart-dependent initialization code
		 ,@(when restart-initialization
		     (list restart-initialization))
		 
		 (setf (trivial-monitored-thread:running this-thread) t)
		 
		 ;; @TODO Enable handling of conditions in the thread's *body* (possibly via handler-bind) for debug environments

		 ,(if *debug-conditions*
		      `(handler-case
			   (unwind-protect
				(iterate:iterate
				  (progn
				    ,@body)
				  (trivial-monitored-thread:inform-thread-alive (trivial-monitored-thread:thread-id this-thread)
										(+ (get-universal-time) (* 2 (max 1 ,sleep-duration))))
				  (iterate:until (trivial-monitored-thread:shutdowndown-requested this-thread))
				  (sleep ,sleep-duration))

			     (setf (trivial-monitored-thread:running this-thread) nil)
			     (setf (trivial-monitored-thread:finished this-thread) t)
			     (incf (trivial-monitored-thread:restart-counter this-thread))

			     ;; When a shutdown was requested, no error/warning must be generated and no restart must happen.
			     (unless (trivial-monitored-thread:shutdowndown-requested this-thread)
			       (log:warn "Ended thread '~a' (id ~a). This thread will~a be restarted."
					 (trivial-monitored-thread:thread-name this-thread)
					 (trivial-monitored-thread:thread-id this-thread)

					 ;; @TODO This is not quite correct, since after bt:destroy-thread would also say 'will be restarted'
					 (if (<= (trivial-monitored-thread:restart-counter this-thread)
						 (trivial-monitored-thread:max-restarts this-thread))
					     ""
					     " not")))

			     ;; Inject restart-dependent cleanup code
			     ,@(when restart-cleanup
				 (list restart-cleanup)))
			 (condition (c)
			   (log:error "Monitored thread threw an error: ~a" c)
			   (break)))
		      `(unwind-protect
			    (iterate:iterate
			      (progn
				,@body)
			      (trivial-monitored-thread:inform-thread-alive (trivial-monitored-thread:thread-id this-thread)
									    (+ (get-universal-time) (* 2 (max 1 ,sleep-duration))))
			      (iterate:until (trivial-monitored-thread:shutdowndown-requested this-thread))
			      (sleep ,sleep-duration))

			 (setf (trivial-monitored-thread:running this-thread) nil)
			 (setf (trivial-monitored-thread:finished this-thread) t)
			 (incf (trivial-monitored-thread:restart-counter this-thread))

			 ;; When a shutdown was requested, no error/warning must be generated and no restart must happen.
			 (unless (trivial-monitored-thread:shutdowndown-requested this-thread)
			   (log:warn "Ended thread '~a' (id ~a). This thread will~a be restarted."
				     (trivial-monitored-thread:thread-name this-thread)
				     (trivial-monitored-thread:thread-id this-thread)

				     ;; @TODO This is not quite correct, since after bt:destroy-thread would also say 'will be restarted'
				     (if (<= (trivial-monitored-thread:restart-counter this-thread)
					     (trivial-monitored-thread:max-restarts this-thread))
					 ""
					 " not")))

			 ;; Inject restart-dependent cleanup code
			 ,@(when restart-cleanup
			     (list restart-cleanup)))))

		 ;; Inject restart-independent cleanup code
		 ,@(when thread-cleanup
		     (list thread-cleanup))))
     (setf (trivial-monitored-thread:raw-thread this-thread) (bt:make-thread (trivial-monitored-thread:thread-body this-thread) :name ,name))
     (trivial-monitored-thread:thread-id this-thread))))

