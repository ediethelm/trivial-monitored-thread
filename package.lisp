(uiop:define-package #:trivial-monitored-thread
  (:documentation "trivial-monitored-thread")
  (:use #:common-lisp)
  (:export #:monitored-thread
	   #:raw-thread
	   #:thread-id
	   #:thread-body
	   #:thread-args
	   #:sleep-duration
	   #:shutdowndown-requested
	   #:finished
	   #:running
	   #:restart-counter
	   #:max-restarts
	   #:thread-name
	   #:inform-thread-alive
	   #:enable-condition-debugging
	   #:disable-condition-debugging
	   #:make-monitored-thread
	   #:register-on-dead-thread
	   #:thread-alive
	   #:start-thread
	   #:stop-thread
	   #:join-thread
	   #:start-thread-monitor
	   #:stop-thread-monitor))
